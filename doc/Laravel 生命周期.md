# Laravel 生命周期

## 1.入口文件（index.php）

## 2.加载项目的依赖（composer autoload）

## 3.创建应用实例（服务容器）

1. 设置应用的基础路径并绑定路径到服务容器
2. 注册基础的服务提供者到服务容器
3. 注册基础的服务到服务容器
4. 注册核心类的别名到服务容器

## 4.内核绑定

- HTTP内核（App\Http\Kernel）

1. 定义了中间件数组

   提供方面过滤HTTP请求

2. 属性bootstrappers设置引导程序

   环境监测

   ​	通过notenv数组把.env文件中的配置加载到$_ENV变量中

   加载配置

   ​	加载config目录下的配置文件

   异常处理

   注册Facade

   ​	用别名的方式快速访问核心类

   注册服务提供者

   ​	把config/app.php目录下的服务器提供者注册到APP容器

- Console内核

## 5.HTTP内核解析

1. ​	通过服务容器make方法把内核解析出来，也就是实例化内核

2. Http\Kernel类的构造器需要服务容器和路由器实例

3. 把内核中定义的中间件组注册到路由器中

   这样就可以在HTTP请求处理前通过这些中间件来实现过滤请求的目的

## 6.创建请求实例

1. 调用Illuminate\Http\Request::capture
2. 在capture方法中使用SymfonyRequest实例提供的内容来创建一个Laravel请求实例

## 7.请求处理

1. 通过HTTP内核的handle方法来处理

   接收HTTP请求，创建HTTP响应实例

2. 具体处理交给sendRequestThroughROuter($request)方法

   1. 将请求实例绑定到服务容器，供以后使用
   2. 清除以前的请求实例缓存
   3. 启动引导程序
      1. 具体由bootstrap方法执行引导程序
      2. 接着会调用APP容器中的bootstrapWith方法
      3. 使用APP容器来解析引导程序
      4. 调用引导程序的bootstrap方法来完成引导程序的启动
   4. 发送请求到路由
      1. 管道（pipeline）创建
      2. 将请求对象传递给管道
      3. 处理请求
         1. 通过请求查找对应路由的实例（findRoute）
         2. 通过实例栈来运行给指定的路由（runRouteWithinStack）
         3. 在路由中执行中间件
         4. 运行routes/web.php配置文件中匹配到的控制器和匿名函数（Route::run()）
         5. f返回响应实例（Route::prepareResponse()）

## 8.发送响应

1. 发送响应内容有Symfony\Componert\HttpFoundation\Response::send()
2. 发送响应手部和响应主题

## 9.终止程序

​	完成终止中间件的调用