##### **Laravel 自带了哪些中间件？ 他们的作用是什么？**

- **Authenticate**：用户身份认证和重定向。
- **EncryptCookies**：cookie 的加解密处理和验证
- **PreventRequestsDuringMaintenance**：维护模式
- **RedirectIfAuthenticated**：注册、登录、忘记密码时如果已经登录则跳转
- **TrimStrings**：前后空白字符清理
- **TrustHosts**：配置了授信任主机的白名单
- **TrustProxies**：配置可信代理
- **VerifyCsrfToken**：处理CSRF验证